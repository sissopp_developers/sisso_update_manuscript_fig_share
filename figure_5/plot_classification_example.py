import shutil
import numpy as np

from sissopp import (
    Inputs,
    FeatureNode,
    FeatureSpace,
    Unit,
    initialize_values_arr,
    SISSOClassifier,
)
import matplotlib.pyplot as plt
import seaborn as sb
import pickle
from pathlib import Path
from sissopp.postprocess.load_models import load_model
import pandas as pd

np.random.seed(13)
plt.rcParams["font.size"] = 10
plt.rcParams["font.family"] = "serif"
plt.rcParams["text.usetex"] = True

colors = [
    "#9C0D38",
    "#1F271B",
    "#EC9192",
    "#FFD23F",
    "#B4654A",
    "#8AC4FF",
    "#6B5CA5",
    "#17B890",
]


def plane(x, coefs=[1.0, 1.0, 1.0]):
    return coefs[0] * x[:, 0] + coefs[1] * x[:, 1] + coefs[2] * x[:, 2]


def get_prop(x, coefs=[[1.0, 1.0, 1.0], [0, 0, 1]]):
    prop = np.zeros(x.shape[0])
    for cc, coef_list in enumerate(coefs):
        prop += 2**cc * (plane(x, coef_list) < 0).astype(int)

    return prop


def get_margin_inds(x, margin_dist=0.6, coefs=[[1.0, 1.0, 1.0], [0, 0, 1]]):
    inds = []
    for coef_list in coefs:
        inds.append(np.where(np.abs(plane(x, coef_list)) < margin_dist)[0])

    return np.unique(np.hstack(inds))


if Path("models/train_dim_3_model_0.dat").exists():
    model_0 = load_model("models/train_dim_3_model_0.dat")
    model_1 = load_model("models/train_dim_3_model_2.dat")
else:
    n_samp = 1000
    n_feats = 2 * 3

    task_sizes_train = [n_samp]
    task_sizes_test = [0]

    initialize_values_arr(task_sizes_train, task_sizes_test, n_feats, 0)

    inputs = Inputs()
    inputs.sample_ids_train = [str(ii) for ii in range(0, n_samp)]
    inputs.sample_ids_test = []

    train_data = np.random.normal(0.0, 0.5, (n_samp, n_feats))
    train_data[:, 3:6] = train_data[:, :3].copy()
    prop = get_prop(train_data)

    margin_dist = 0.6
    inds_margin = get_margin_inds(train_data, margin_dist)

    sample_pts = np.random.normal(0.0, 0.5, (1000 * n_samp, n_feats))

    sample_margin = get_margin_inds(sample_pts, margin_dist)
    sample_pts = np.delete(sample_pts, sample_margin, axis=0)
    prop_samp_pts = get_prop(sample_pts)

    inds = [inds_margin[np.where(prop[inds_margin] == ii)[0]] for ii in range(4)]
    inds_samp = [np.where(prop_samp_pts == ii)[0] for ii in range(4)]

    for ind_list, ind_samp_list in zip(inds, inds_samp):
        replace_inds = np.random.choice(len(ind_samp_list), len(ind_list), False)
        train_data[ind_list, :3] = sample_pts[ind_samp_list[replace_inds], :3]

    prop_test_change = get_prop(train_data)
    assert np.all(prop_test_change == prop)
    inputs.phi_0 = [
        FeatureNode(
            ff,
            f"x_{ff}",
            train_data[:, ff],
            [],
            Unit(),
        )
        for ff in range(n_feats)
    ]

    inputs.allowed_ops = ["add", "sub", "mult", "sq", "cb", "sqrt", "cbrt"]
    inputs.calc_type = "classification"
    inputs.n_sis_select = n_feats
    inputs.override_n_sis_select = True
    inputs.n_dim = 3
    inputs.n_residual = 1
    inputs.n_models_store = n_feats
    inputs.task_names = ["all"]
    inputs.task_sizes_train = task_sizes_train
    inputs.task_sizes_test = task_sizes_test
    inputs.leave_out_inds = []
    inputs.prop_label = "Class"

    inputs.prop_train = prop
    feat_space = FeatureSpace(inputs)
    sisso = SISSOClassifier(inputs, feat_space)
    sisso.fit()

    model_0 = sisso.models[-1][0]
    model_1 = sisso.models[-1][2]

inds_0 = np.where(model_0.prop_train == 0)
inds_1 = np.where(model_0.prop_train == 1)
inds_2 = np.where(model_0.prop_train == 2)
inds_3 = np.where(model_0.prop_train == 3)

models = [model_0, model_1]

plt.rcParams.update(
    {
        "figure.facecolor": (1.0, 1.0, 1.0, 0.0),  # red   with alpha = 30%
        "axes.facecolor": (1.0, 1.0, 1.0, 0.0),  # green with alpha = 50%
        "savefig.facecolor": (1.0, 1.0, 1.0, 0.0),  # blue  with alpha = 20%
    }
)

fig, axs = plt.subplots(
    nrows=2, ncols=1, figsize=[3.0, 4.0], subplot_kw={"projection": "3d"}
)

fig.subplots_adjust(
    top=0.975, bottom=0.063, left=0.05, right=0.8, wspace=0.2, hspace=0.0
)

labels = ["a)", "b)"]
base_x = [0, 3]
order = [[2, 1, 0], [2, 1, 0]]

for aa, ax in enumerate(axs.flatten()):
    ax.tick_params(direction="in", which="both", right=True, top=True, zorder=1000)
    ax.set_xlim([-2.05, 2.05])
    ax.set_ylim([-2.05, 2.05])
    ax.set_zlim([-2.05, 2.05])

    ax.set_xlabel(f"$x_{base_x[aa]}$")
    ax.set_ylabel(f"$x_{base_x[aa] + 1}$")
    ax.set_zlabel(f"$x_{base_x[aa] + 2}$")

    ax.set_xticks(np.arange(-2.0, 2.05, 1.0))
    ax.set_xticks(np.arange(-2.0, 2.05, 0.25), minor=True)

    ax.set_yticks(np.arange(-2.0, 2.05, 1.0))
    ax.set_yticks(np.arange(-2.0, 2.05, 0.25), minor=True)

    ax.set_zticks(np.arange(-2.0, 2.05, 1.0))
    ax.set_zticks(np.arange(-2.0, 2.05, 0.25), minor=True)

    ax.text(3.0, 3.0, 3.0, labels[aa])

    model = models[aa]
    coefs = model.coefs[0]

    mesh = np.linspace(-1.1, 1.1, 10)
    xx, yy = np.meshgrid(mesh, mesh)
    zz = -1.0 * (xx + yy) / 1.0

    ax.plot_surface(xx, yy, np.zeros(xx.shape), color="#00F8BA")
    ax.plot_surface(xx, yy, zz, color="#00F8BA")
    ax.scatter(
        model.feats[order[aa][0]].value[inds_0],
        model.feats[order[aa][1]].value[inds_0],
        model.feats[order[aa][2]].value[inds_0],
        ".",
        c="#8AC4FF",
    )

    ax.scatter(
        model.feats[order[aa][0]].value[inds_1],
        model.feats[order[aa][1]].value[inds_1],
        model.feats[order[aa][2]].value[inds_1],
        ".",
        c="#9C0D38",
    )
    ax.scatter(
        model.feats[order[aa][0]].value[inds_2],
        model.feats[order[aa][1]].value[inds_2],
        model.feats[order[aa][2]].value[inds_2],
        ".",
        c="#000000",
    )

    ax.scatter(
        model.feats[order[aa][0]].value[inds_3],
        model.feats[order[aa][1]].value[inds_3],
        model.feats[order[aa][2]].value[inds_3],
        ".",
        c="#CC8614",
    )
    ax.view_init(7.5, 145)

fig.savefig("figure_5.pdf")
