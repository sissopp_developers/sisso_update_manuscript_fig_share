# SISSO Update Manuscript Fig Share

## Setting up the Python Environment

The python environment for the papers is the same as needed for SISSO++, see [here](https://sissopp_developers.gitlab.io/sissopp/quick_start/Installation.html) for more details.
