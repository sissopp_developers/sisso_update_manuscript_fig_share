from sissopp.py_interface import get_fs_solver, read_csv
from sissopp.postprocess.plot import plt
from sissopp.postprocess.load_models import load_model

import numpy as np
import pandas as pd
from pathlib import Path
import os

plt.rcParams["font.size"] = 8
plt.rcParams["font.family"] = "serif"

np.random.seed(42)

allowed_ops = [
    "add",
    "sub",
    "mult",
    "div",
    "abs_diff",
    "inv",
    "abs",
    "cos",
    "sin",
    "exp",
    "neg_exp",
    "log",
    "sq",
    "sqrt",
    "cb",
    "cbrt",
    "sin",
    "cos",
    "six_pow",
]

allowed_p_ops = ["inv", "sq", "sin"]

path_lor = Path("lorentz")
if not path_lor.exists():
    path_lor.mkdir()
    os.chdir(path_lor)
    data = np.zeros((1000, 2))
    data[:, 1] = np.random.uniform(-10, 10, (1000))
    data[:, 0] = 1.0 / ((data[:, 1] - 2.5) ** 2.0 + 1.0) + np.random.normal(
        0.0, 0.02, (1000)
    )

    df = pd.DataFrame(data=data, columns=["y", "x1"])
    df.to_csv("data.csv", index_label="ID")

    inputs = read_csv(df, "y", max_rung=2)
    inputs.n_sis_select = 100
    inputs.allowed_ops = allowed_ops
    inputs.calc_type = "regression"

    path = Path("no_param")
    path.mkdir(exist_ok=True)
    os.chdir(path)
    fs_lor_np, sr_lor_np = get_fs_solver(inputs)
    sr_lor_np.fit()
    model_lor_np = sr_lor_np.models[-1][0]
    os.chdir("../")

    inputs = read_csv(df, "y", max_rung=2)
    inputs.n_sis_select = 100
    inputs.allowed_ops = allowed_ops
    inputs.allowed_param_ops = allowed_p_ops
    inputs.calc_type = "regression"
    inputs.global_param_opt = True

    path = Path("param")
    path.mkdir(exist_ok=True)
    os.chdir(path)
    fs_lor_p, sr_lor_p = get_fs_solver(inputs)
    sr_lor_p.fit()
    os.chdir("../../")
    model_lor_p = sr_lor_p.models[-1][0]
else:
    model_lor_np = load_model(f"{path_lor}/no_param/models/train_dim_1_model_0.dat")
    model_lor_p = load_model(f"{path_lor}/param/models/train_dim_1_model_0.dat")

path_sin = Path("sin")
if not path_sin.exists():
    path_sin.mkdir()
    os.chdir(path_sin)
    data = np.zeros((1000, 2))
    data[:, 1] = np.random.uniform(-10, 10, (1000))
    data[:, 0] = np.sin(1.5 * data[:, 1] + np.pi / 6.0) + np.random.normal(
        0.0, 0.5, (1000)
    )
    df = pd.DataFrame(data=data, columns=["y", "x1"])
    df.to_csv("data.csv", index_label="ID")

    inputs = read_csv(df, "y", max_rung=2)
    inputs.n_sis_select = 100
    inputs.allowed_ops = allowed_ops
    inputs.calc_type = "regression"

    path = Path("no_param")
    path.mkdir(exist_ok=True)
    os.chdir(path)
    fs_sin_np, sr_sin_np = get_fs_solver(inputs)
    sr_sin_np.fit()
    model_sin_np = sr_sin_np.models[-1][0]
    os.chdir("../")

    inputs = read_csv(df, "y", max_rung=2)
    inputs.n_sis_select = 100
    inputs.allowed_ops = allowed_ops
    inputs.allowed_param_ops = allowed_p_ops
    inputs.calc_type = "regression"
    inputs.max_param_depth = 1
    path = Path("param")
    path.mkdir(exist_ok=True)
    os.chdir(path)
    fs_sin_p, sr_sin_p = get_fs_solver(inputs)
    sr_sin_p.fit()
    model_sin_p = sr_sin_p.models[-1][0]
    os.chdir("../../")
else:
    model_sin_np = load_model(f"{path_sin}/no_param/models/train_dim_1_model_0.dat")
    model_sin_p = load_model(f"{path_sin}/param/models/train_dim_1_model_0.dat")

path_lor_noise = Path("lor_noise")
if not path_lor_noise.exists():
    path_lor_noise.mkdir()
    os.chdir(path_lor_noise)
    data = np.zeros((1000, 2))
    data[:, 1] = np.random.uniform(-10, 10, (1000))
    data[:, 0] = data[:, 0] = 1.0 / (
        (data[:, 1] - 4.5) ** 2.0 + 0.25
    ) + np.random.normal(0.0, 0.5, (1000))
    df = pd.DataFrame(data=data, columns=["y", "x1"])
    df.to_csv("data.csv", index_label="ID")

    inputs = read_csv(df, "y", max_rung=2)
    inputs.n_sis_select = 100
    inputs.allowed_ops = allowed_ops
    inputs.calc_type = "regression"

    path = Path("no_param")
    path.mkdir(exist_ok=True)
    os.chdir(path)
    fs_sin_np, sr_sin_np = get_fs_solver(inputs)
    sr_sin_np.fit()
    model_lor_noise_np = sr_sin_np.models[-1][0]
    os.chdir("../")

    inputs = read_csv(df, "y", max_rung=2)
    inputs.n_sis_select = 100
    inputs.allowed_ops = allowed_ops
    inputs.allowed_param_ops = allowed_p_ops
    inputs.calc_type = "regression"
    inputs.max_param_depth = 1
    path = Path("param")
    path.mkdir(exist_ok=True)
    os.chdir(path)
    fs_sin_p, sr_sin_p = get_fs_solver(inputs)
    sr_sin_p.fit()
    model_lor_noise_p = sr_sin_p.models[-1][0]
    os.chdir("../../")
else:
    model_lor_noise_np = load_model(
        f"{path_lor_noise}/no_param/models/train_dim_1_model_0.dat"
    )
    model_lor_noise_p = load_model(
        f"{path_lor_noise}/param/models/train_dim_1_model_0.dat"
    )

df_lor = pd.read_csv("lorentz/data.csv", index_col=0)
df_sin = pd.read_csv("sin/data.csv", index_col=0)
df_lor_noise = pd.read_csv("lor_noise/data.csv", index_col=0)

fig, axs = plt.subplots(nrows=3, ncols=2, figsize=[4.25, 4.25])
ax1, ax2, ax3, ax4, ax5, ax6 = axs.flatten()
fig.subplots_adjust(
    top=0.90, bottom=0.10, left=0.15, right=0.975, wspace=0.15, hspace=0.15 * 8.4 / 5.75
)

for ax in axs.flatten():
    ax.tick_params(direction="in", which="both", right=True, top=True, zorder=1000)
    ax.set_xlim([-10, 10])
    ax.set_xticks(np.arange(-10, 11, 5))
    ax.set_xticks(np.arange(-10, 11, 1), minor=True)

for ax in [ax1, ax2]:
    ax.set_xticklabels([""] * 5)
    ax.set_ylim([-0.10, 1.10])
    ax.set_yticks(np.arange(0.0, 1.11, 0.25))
    ax.set_yticks(np.arange(-0.1, 1.11, 0.05), minor=True)
    ax.plot(df_lor["x1"], df_lor["y"], ".", color="#47498B")

for ax in [ax3, ax4]:
    ax.set_ylim([-2.5, 2.5])
    ax.set_yticks(np.arange(-2.0, 2.01, 1.0))
    ax.set_yticks(np.arange(-2.5, 2.51, 0.25), minor=True)
    ax.plot(df_sin["x1"], df_sin["y"], ".", color="#47498B")


for ax in [ax5, ax6]:
    ax.set_ylim([-1.75, 4.75])
    ax.set_yticks(np.arange(-1.0, 4.76, 1.0))
    ax.set_yticks(np.arange(-1.75, 4.76, 0.25), minor=True)
    ax.plot(df_lor_noise["x1"], df_lor_noise["y"], ".", color="#47498B")

ax2.set_yticklabels([""] * 5)
ax4.set_yticklabels([""] * 5)
ax6.set_yticklabels([""] * 6)

plt.rcParams["font.size"] = 8

ax1.set_ylabel("y $\\left[\\frac{1}{\\left(x - 2.5\\right)^2 + 1}\\right]$")
ax3.set_ylabel("y $\\left[\\sin\\left(1.5 x + \\frac{\\pi}{6}\\right)\\right]$")
ax5.set_ylabel("y $\\left[\\frac{1}{\\left(x - 4.5\\right)^2 + 0.25}\\right]$")
ax5.set_xlabel("x")
ax6.set_xlabel("x")

x_inds = df_lor["x1"].argsort()
ax1.plot(
    df_lor.loc[x_inds, "x1"],
    model_lor_np.fit[x_inds],
    color="#FF4D7C",
    linewidth=2.0,
)
ax2.plot(
    df_lor.loc[x_inds, "x1"],
    model_lor_p.fit[x_inds],
    color="#FF4D7C",
    linewidth=2.0,
)

x_inds = df_sin["x1"].argsort()
ax3.plot(
    df_sin.loc[x_inds, "x1"],
    model_sin_np.fit[x_inds],
    color="#FF4D7C",
    linewidth=2.0,
)
ax4.plot(
    df_sin.loc[x_inds, "x1"],
    model_sin_p.fit[x_inds],
    color="#FF4D7C",
    linewidth=2.0,
)


x_inds = df_lor_noise["x1"].argsort()
ax5.plot(
    df_lor_noise.loc[x_inds, "x1"],
    model_lor_noise_np.fit[x_inds],
    color="#FF4D7C",
    linewidth=2.0,
)
ax6.plot(
    df_lor_noise.loc[x_inds, "x1"],
    model_lor_noise_p.fit[x_inds],
    color="#FF4D7C",
    linewidth=2.0,
)

ax1.text(0.0, 1.20, "Non-Parametric", ha="center")
ax2.text(0.0, 1.20, "Parametric", ha="center")

ax1.text(-9.25, 0.95, "a)")
ax2.text(-9.25, 0.95, "b)")
ax3.text(-9.25, 1.9, "c)")
ax4.text(-9.25, 1.9, "d)")
ax5.text(-9.25, 4.0, "e)")
ax6.text(-9.25, 4.0, "f)")

fig.savefig("figure_3.pdf")
