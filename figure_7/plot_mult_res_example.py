from sissopp.py_interface import get_fs_solver, read_csv
from sissopp.postprocess.plot import plt
from sissopp.postprocess.load_models import load_model

import numpy as np
import pandas as pd
from pathlib import Path
import os

import contextlib
import shutil
import tempfile

np.random.seed(42)

base_path = Path(__file__).parent

allowed_ops = [
    "add",
    "sub",
    "mult",
    "abs_diff",
    "div",
    "sin",
    "inv",
    "abs",
    "sq",
    "cb",
    "sqrt",
    "cbrt",
    "six_pow",
]

plt.rcParams["font.size"] = 8
plt.rcParams["font.family"] = "serif"

path = Path(f"{base_path}/workdir/")
path.mkdir(exist_ok=True)
os.chdir(path)

data = np.random.uniform(-10, 10, (100, 15))

a0 = 5.5
a1 = 4.158e-1
a2 = -9.74e-2

f1 = (data[:, 0]) ** 2.0 * np.cbrt(data[:, 1])
f2 = np.abs(data[:, 2] ** 3.0)
noise = np.random.normal(0.0, 0.05, (100))
y = a0 + a1 * f1 + a2 * f2 + noise

data[:, 3] = (y + np.random.normal(0.0, 20.0, (100))) ** -3.0
df = pd.DataFrame(
    data=np.column_stack((y, data)), columns=["y"] + [f"x_{ii}" for ii in range(15)]
)
df.to_csv("data.csv")

model_list = []

# sis_range = [100]
sis_range = [50, 100, 200, 400, 800, 1600]
res_range = [1, 25, 50]
for rr in res_range:
    model_list.append([])
    for ss in sis_range:
        work_path = path / f"n_sis_{ss}/n_res_{rr}"
        work_path.mkdir(exist_ok=True, parents=True)
        os.chdir(work_path)
        if not Path("models/train_dim_2_model_0.dat").exists():
            inputs = read_csv(df, "y", max_rung=2)
            inputs.n_sis_select = ss
            inputs.n_dim = 2
            inputs.allowed_ops = allowed_ops
            inputs.calc_type = "regression"
            inputs.n_residual = rr

            fest_space, solver = get_fs_solver(inputs, allow_overwrite=True)
            solver.fit()

        model_list[-1].append(load_model("models/train_dim_2_model_0.dat"))
        os.chdir(path)

os.chdir(base_path)
fig, ax = plt.subplots(nrows=1, ncols=1, figsize=[3.0, 2.0])
fig.subplots_adjust(
    top=0.95, bottom=0.15, left=0.125, right=0.95, wspace=0.15, hspace=0.15 * 8.4 / 5.75
)
ax.set_xlim([50, 1600])
ax.set_xscale("log")
ax.tick_params(direction="in", which="both", right=True, top=True, zorder=1000)
ax.set_xticks(sis_range)
ax.set_xticklabels([str(ss) for ss in sis_range])
ax.set_xticks([], minor=True)
ax.set_xlabel("n_sis_select")

ax.set_ylabel("Training RMSE")

ax.plot(
    sis_range,
    [model.rmse for model in model_list[0]],
    color="#122C34",
    label="n_res = 1",
)
ax.plot(
    sis_range,
    [model.rmse for model in model_list[1]],
    dashes=(6, 6),
    dash_capstyle="butt",
    color="#39A2AE",
    label="n_res = 25",
)
ax.plot(
    sis_range,
    [model.rmse for model in model_list[2]],
    color="#A40E4C",
    label="n_res = 50",
)

ax.legend(frameon=False)

fig.savefig("figure_7.pdf")
